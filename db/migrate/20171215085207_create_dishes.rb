class CreateDishes < ActiveRecord::Migration[5.1]
  def up
    create_table :dishes do |t|
      t.string :name
      t.text :description
      t.integer :menus_appeared, default: true
      t.integer :times_appeared
      t.integer :first_appeared
      t.integer :last_appeared
      t.decimal :lowest_price, precision: 10, scale: 2, default: 0.00
      t.decimal :highest_price, precision: 10, scale: 2, default: 0.00

      t.timestamps
    end
  end

  def down
    drop_table :dishes
  end
end
