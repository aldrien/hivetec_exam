class ChangeMenuDateColumnTypeToDate < ActiveRecord::Migration[5.1]
  def up
  	change_column :menus, :date, :date
  end

  def down
  	change_column :menus, :date, :string
  end
end