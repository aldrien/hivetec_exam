class CreateMenuItems < ActiveRecord::Migration[5.1]
  def up
    create_table :menu_items do |t|
    	t.integer :menu_page_id 
			t.decimal :price 
			t.decimal :high_price
			t.integer :dish_id
			t.float :xpos
			t.float :ypos

      t.timestamps
    end
  end

  def down
  	drop_table :menu_items
	end
end
