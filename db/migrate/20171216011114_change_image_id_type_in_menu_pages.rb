class ChangeImageIdTypeInMenuPages < ActiveRecord::Migration[5.1]
  def up
  	change_column :menu_pages, :image_id, :bigint
  end

  def down
  	change_column :menu_pages, :image_id, :integer
  end
end
