class ImportCsvJob < ApplicationJob
  queue_as :import_csv

  def perform(file, table)
  	Rails.logger.debug "#{self.class.name}: I'm performing my job!"
  	
  	csv_file = "#{Rails.root.join('public', 'csv', file)}" # "#{Rails.public_path}/#{file}"

    CSV.foreach(csv_file, headers: true) do |row|
    	case table
    	when 'dish'
	    	Dish.create! row.to_hash
    	when 'menu'
    		Menu.create! row.to_hash
  		when 'menu_item'
  			MenuItem.create! row.to_hash
			when 'menu_page'
				MenuPage.create! row.to_hash
    	end	
	  end
  end
end
