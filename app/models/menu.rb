class Menu < ApplicationRecord
	has_many :menu_pages
	has_many :menu_items, through: :menu_pages

	# I temporarily removed the validation for the purpose of uploading the given CSV records
	# Required columns/fields are to be confirmed with the Client in future development.
	validates_presence_of :name, :sponsor, :event, :venue, :place

	scope :by_place, -> (place) { where("place LIKE ? || location LIKE ?", "%#{place}%", "%#{place}%")}
	scope :by_date, -> (from, to) { where("DATE(date) BETWEEN ? AND ?", from, to) }
end