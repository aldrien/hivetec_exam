class Dish < ApplicationRecord
	has_many :menu_items

	# I temporarily removed the validation for the purpose of uploading the given CSV records
	# Required columns/fields are to be confirmed with the Client in future development.
	# validates_presence_of :name, :description

	## Importing is now done in background job
	# def self.import(file)
  #  	CSV.foreach(file.path, headers: true) do |row|
	#     Dish.create! row.to_hash
	#   end
	# end

	class << self
		def filter(keyword)
			where("name LIKE :key || description LIKE :key", key: "%#{keyword}%")
		end
	end
end
