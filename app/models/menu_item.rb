class MenuItem < ApplicationRecord
	# If you remove the optional: true, it will automatically requires the id
	# I added optional since records show that is not required.
	
	belongs_to :dish, optional: true
	belongs_to :menu, optional: true
	belongs_to :menu_page, optional: true

	# I temporarily removed the validation for the purpose of uploading the given CSV records
	# Required columns/fields are to be confirmed with the Client in future development.
	# validates_presence_of :price, :high_price
end
