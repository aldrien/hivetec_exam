class MenuPage < ApplicationRecord
	belongs_to :menu, optional: true
	has_many :menu_items

	# I temporarily removed the validation for the purpose of uploading the given CSV records
	# Required columns/fields are to be confirmed with the Client in future development.
	# validates_presence_of :page_number
end
