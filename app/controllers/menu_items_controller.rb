class MenuItemsController < ApplicationController
  def index
    @menu_items = MenuItem.order(sort_column + " " + sort_direction).paginate(:page => params[:page], :per_page => 10)
  end

  def create
    # Since MenuItem.csv need to be uploaded, I temporarily increase the allowed file size.
    if params[:file].size > 1200000000 #25000000 #25 MB
      flash[:error] = 'Sorry, File size should not be more than 25MB!'
      redirect_to new_menu_item_url
    else
    	begin
    		csv_file = params[:file]
        upload_csv_file(csv_file) # see application_controller
        ImportCsvJob.perform_later(csv_file.original_filename, 'menu_item')
    		
        flash[:notice] = "Menu Item CSV was successfully uploaded and will be saved in background."
        redirect_to menu_items_url
      rescue => error
      	flash[:error] = "Failed to upload CSV File! Error: #{error.message}"
        render :new
    	end
    end
  end
private  
  def sort_column
    MenuItem.column_names.include?(params[:sort]) ? params[:sort] : "menu_page_id"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end 
end
# Only used for clearing table in development.
# ActiveRecord::Base.connection.execute("TRUNCATE menu_items")