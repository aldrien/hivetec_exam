class DishesController < ApplicationController
  def index
    if params[:keyword].present?
      @dishes = Dish.filter(params[:keyword])
    else
      @dishes = Dish.all
    end
    @dishes = @dishes.order(sort_column + " " + sort_direction).paginate(:page => params[:page], :per_page => 10)
  end

  def new
  end

  def create
    # Now moves to background job
    # @dish = Dish.import(params[:file])

    # Since Dish.csv need to be uploaded, I temporarily increase the allowed file size.
    if params[:file].size > 27000000 #25000000 #25 MB
      flash[:error] = 'Sorry, File size should not be more than 25MB!'
      redirect_to new_dish_url
    else
      begin
        csv_file = params[:file]
        upload_csv_file(csv_file) # see application_controller
        ImportCsvJob.perform_later(csv_file.original_filename, 'dish')
        
        flash[:notice] = "Dish CSV was successfully uploaded and will be saved in background."
        redirect_to dishes_url
      rescue => error
        flash[:error] = "Failed to upload CSV File! Error: #{error.message}"
        render :new
      end
    end
  end

private  
  def sort_column
    Dish.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end  
end
# Only used for clearing table in development.
# ActiveRecord::Base.connection.execute("TRUNCATE dishes")