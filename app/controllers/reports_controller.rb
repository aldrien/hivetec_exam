class ReportsController < ApplicationController
  def index
  end

  def download_report
  	@menus = Menu.all
  	
  	# Start Filtering
  	if params[:date_from].present? && params[:date_to].present?
			@menus = @menus.by_date(convert_hash_date(params[:date_from]), convert_hash_date(params[:date_to]))
		elsif params[:place].present?
			@menus = @menus.by_place(params[:place])
		end
		
   	# No filtered result notification
   	if @menus.empty?
	  	flash[:error] = 'Sorry, no record found!'
	  	redirect_to json_export_path and return 
	  end

	  # Render JSON File to download
   	send_data @menus.to_json, filename: 'menu_events.json'
	end

private
	def convert_hash_date(date)
		Date.new(*date.values.map(&:to_i)).to_time
	end
end
