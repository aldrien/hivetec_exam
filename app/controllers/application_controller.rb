class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include ActionView::Helpers::NumberHelper

  helper_method :sort_column, :sort_direction 

  def upload_csv_file(csv)
    File.open(Rails.root.join('public', 'csv', csv.original_filename), 'wb') do |file|
      file.write(csv.read)
    end
  end
end
