class MenusController < ApplicationController
  def index
    @menus = Menu.order(sort_column + " " + sort_direction).paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @menu = Menu.find(params[:id])
  end

  def create
    if params[:file].size > 5000000 #5 MB
      flash[:error] = 'Sorry, File size should not be more than 5MB!'
      redirect_to new_menu_item_url
    else
      begin
        csv_file = params[:file]
        upload_csv_file(csv_file) # see application_controller
        ImportCsvJob.perform_later(csv_file.original_filename, 'menu')
        
        flash[:notice] = "Menu CSV was successfully uploaded and will be saved in background."
        redirect_to menus_url
      rescue => error
        flash[:error] = "Failed to upload CSV File! Error: #{error.message}"
        render :new
      end
    end
  end
private  
  def sort_column
    Menu.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end 
end
# Only used for clearing table in development.
# ActiveRecord::Base.connection.execute("TRUNCATE menus")