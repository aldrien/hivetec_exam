# PLEASE README

This README would normally document whatever steps are necessary to get the
application up and running.

## Local Set Up

* Go to application directory in Terminal
```cd app_dir```

* Install local gems and dependencies
```bundle install```

* Update Database configuration YML file
```app_dir/config/database.yml```

* Set up local database (does db:create, db:schema:load, db:seed)
```rails db:setup```

* Run App locally
```rails server or rails s```

* Visit link in browser
```http://localhost:3000```

## Install and config Redis on Mac OS X via Homebrew (in my case I used OSX)
* For more info
	Visit: https://medium.com/@petehouston/install-and-config-redis-on-mac-os-x-via-homebrew-eb8df9a4f298

* Install via brew
```brew update```
```brew install redis```

* Launch Redis on computer starts.
```ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents```

* Start Redis server via “launchctl”.
```launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist```

* Test if Redis server is running.
```redis-cli ping```

	If it replies “PONG”, then it’s good to go!

## Resque & Redis: Run the following in different tab or terminal, for background jobs to work
* Run Redis server (let say you're not running redis at the moment....)
```redis-server /usr/local/etc/redis.conf```

* Run the following in console (otherwise your workers will not start (just pending))

- For single worker:
```rake resque:work QUEUE=* ```

- For multiple workers, does simultaneous process in background (change COUNT value)
```rake resque:workers QUEUE='*' COUNT='3'```

* For more info
Visit: https://github.com/resque/resque/wiki/ActiveJob

## Open Resque Web Server/Admin
* http://localhost:3000/jobs

## Unit Test

* Run RSPEC tests (open new tab from Terminal)
	- For all tests:
	```rspec```

	- For specific job test:
	```rspec spec/jobs/import_csv_job_spec.rb```

	- For specific controller test:
	```rspec spec/controllers/dishes_controller_spec.rb```

	- For specific featur test:
	```rspec spec/features/dishes_feature.rb```

	- For specific model test:
	```rspec spec/model/dish_spec.rb```
	
	Test files are located in:
	```app_dir/spec/```

## Rubocop Gem (Ruby Static Code Analyzer)
* How to run
```cd app_dir```
```rubocop```

## Additional Info
* Check if Resque jobs are working properly(please supply proper parameters)
```rails runner "ImportCsvJob.perform_later(1,2)" && tail -n 1 log/development.log```


## Future Developments
* Fix violations based on "Rubocop" gem
* Add searches on other page (currently only in Dishes)
* Add tests for JSON Exporting and searches
* Create new model `cvs_upload` for handling CSV files, then use `gem paperclip` for proper validations
