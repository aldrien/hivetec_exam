Rails.application.routes.draw do
	require 'resque/server'
	mount Resque::Server, at: '/jobs'

	resources :dishes
	resources :menus
	resources :menu_items
	resources :menu_pages

	match 'json_export' => 'reports#index', as: :json_export, via: :get
	match 'download_report' => 'reports#download_report', as: :download_report, via: :post
	
  root to: 'home#index'
end
