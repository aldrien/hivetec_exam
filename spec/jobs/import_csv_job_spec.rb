require 'rails_helper'

RSpec.describe ImportCsvJob, type: :job do
  describe "#perform_later" do
    it "importing CSV records to DB" do
      ActiveJob::Base.queue_adapter = :test

      expect {
        ImportCsvJob.perform_later('Dish.csv', 'dish')
      }.to have_enqueued_job
    end
  end
end
