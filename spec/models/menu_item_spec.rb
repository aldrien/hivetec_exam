require 'rails_helper'

RSpec.describe MenuItem, type: :model do
  it 'should belongs to dish' do
  	# Automatically creates dish record, due to factory association
  	@menu_item = create(:menu_item)

  	expect(@menu_item.dish.name).to eq('Consomme printaniere royal')
  end
end
