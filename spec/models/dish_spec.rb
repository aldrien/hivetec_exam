require 'rails_helper'

RSpec.describe Dish, type: :model do
  it 'has many menu items' do
  	# Automatically creates dish record, due to factory association
  	@menu_item = create(:menu_item)
  	@dish = Dish.last

  	expect(@menu_item.dish_id).to eq(@dish.id)
  	expect(@menu_item.price).to eq(@dish.menu_items.last.price)
  	expect(@menu_item.high_price).to eq(@dish.menu_items.last.high_price)
	end
end
