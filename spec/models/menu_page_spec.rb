require 'rails_helper'

RSpec.describe MenuPage, type: :model do
  it 'has many menu_items and belongs_to menu' do
  	FactoryBot.define do
		  factory :new_menu_item, class: 'MenuItem' do
				price '2.50'
				high_price '3.50'
				association :dish, factory: :dish
			end
		end

  	# This creates dish, menu_page, menu
  	@menu_item = create(:menu_item)
  	FactoryBot.create(:new_menu_item, menu_page: MenuPage.last)
  	@menu_page = MenuPage.last

  	expect(@menu_page.menu.name).to eq('Menu Name')
  	expect(@menu_page.menu.sponsor).to eq('HOTEL EASTMAN')
  	expect(@menu_page.menu_items.count).to eq(2)
	end
end
