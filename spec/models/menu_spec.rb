require 'rails_helper'

RSpec.describe Menu, type: :model do
  it 'has many menu_items thru menu_pages' do
  	# This creates associated records (menu_page, dish, menu)
  	@menu_item = create(:menu_item)
  	@menu = Menu.first

  	expect(@menu.menu_items.count).to eq(1)
  	expect(@menu.menu_pages.count).to eq(1)
  end
end
