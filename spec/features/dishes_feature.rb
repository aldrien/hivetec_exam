require "rails_helper"

RSpec.feature "Dish CSV Upload", :type => :feature do
  before do
    @file = Rails.root.join('spec/fixtures/files/Dish.csv')
  end

  it "can upload csv file" do
    visit new_dish_path
    expect(page).to have_content 'Upload Dish Record'

    # Upload CSV File
    attach_file('file', @file)
    click_button 'Import CSV' 

    # Success Notice
    expect(page).to have_content 'Dish CSV was successfully uploaded and will be saved in background.'
  end
end

# ALIASES

# feature == describe
# :type => :feature
# background == before
# scenario == it
# given/given! == let/let!