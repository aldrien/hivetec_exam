require "rails_helper"

RSpec.feature "Search Dish Record", :type => :feature do
  before do
    create(:dish)
  end

  it "will return some record" do
    visit dishes_path
    expect(page).to have_content 'Listing All Dishes'

    # Enter and search keyword
    fill_in 'keyword', with: 'Consomme'
    click_button 'Search-Now' 

    # Filtered Dishes
    expect(page).to have_content 'Consomme printaniere royal'
    expect(page).to have_content 'sample dish'
  end

  it "will not return records" do
    visit dishes_path
    expect(page).to have_content 'Listing All Dishes'

    # Enter and search keyword
    fill_in 'keyword', with: 'no more'
    click_button 'Search-Now' 

    # No Filtered Dishes
    expect(page).not_to have_content 'Consomme printaniere royal'
  end
end

# ALIASES

# feature == describe
# :type => :feature
# background == before
# scenario == it
# given/given! == let/let!