require "rails_helper"

RSpec.feature "Menu-Item CSV Upload", :type => :feature do
  before do
    @file = Rails.root.join('spec/fixtures/files/MenuItem.csv')
  end

  it "can upload csv file" do
    visit new_menu_item_path
    expect(page).to have_content 'Upload Menu-Item Record'

    # Upload CSV File
    attach_file('file', @file)
    click_button 'Import CSV' 

    # Success Notice
    expect(page).to have_content 'Menu Item CSV was successfully uploaded and will be saved in background.'
  end
end

# ALIASES

# feature == describe
# :type => :feature
# background == before
# scenario == it
# given/given! == let/let!