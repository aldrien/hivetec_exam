require "rails_helper"

RSpec.feature "Menu CSV Upload", :type => :feature do
  before do
    @file = Rails.root.join('spec/fixtures/files/Menu.csv')
  end

  it "can upload csv file" do
    visit new_menu_path
    expect(page).to have_content 'Upload Menu Record'

    # Upload CSV File
    attach_file('file', @file)
    click_button 'Import CSV' 

    # Success Notice
    expect(page).to have_content 'Menu CSV was successfully uploaded and will be saved in background.'
  end
end

# ALIASES

# feature == describe
# :type => :feature
# background == before
# scenario == it
# given/given! == let/let!