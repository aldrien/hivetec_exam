require "rails_helper"

RSpec.feature "Menu-Page CSV Upload", :type => :feature do
  before do
    @file = Rails.root.join('spec/fixtures/files/MenuPage.csv')
  end

  it "can upload csv file" do
    visit new_menu_page_path
    expect(page).to have_content 'Upload Menu-Page Record'

    # Upload CSV File
    attach_file('file', @file)
    click_button 'Import CSV' 

    # Success Notice
    expect(page).to have_content 'Menu Page CSV was successfully uploaded and will be saved in background.'
  end

  it "cannot upload csv file with more than 5 MB size" do
    visit new_menu_page_path
    expect(page).to have_content 'Upload Menu-Page Record'

    # Cannot Upload CSV File
    attach_file('file', Rails.root.join('spec/fixtures/files/MoreThan5Mb.csv'))
    click_button 'Import CSV' 

    # Error Notice
    expect(page).to have_content 'Sorry, File size should not be more than 5MB!'
  end
end

# ALIASES

# feature == describe
# :type => :feature
# background == before
# scenario == it
# given/given! == let/let!