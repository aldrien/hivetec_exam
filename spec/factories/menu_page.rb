FactoryBot.define do
  factory :menu_page do
		association :menu, factory: :menu
		page_number '100'
		image_id '222'
		full_height '7230'
		full_width '5428'
		uuid '510d47e4-2955-a3d9-e040-e00a18064a99'
	end
end