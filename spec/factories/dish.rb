FactoryBot.define do
  factory :dish do
    name 'Consomme printaniere royal'
    description 'sample dish'
    menus_appeared '8'
    times_appeared  '9'
    first_appeared '1897'
    last_appeared '1927'
    lowest_price '0.2'
    highest_price '0.4'
  end
end
