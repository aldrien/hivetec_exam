FactoryBot.define do
  factory :menu_item do
		association :menu_page, factory: :menu_page
		price '2.50'
		high_price '3.50'
		association :dish
	end
end