FactoryBot.define do
  factory :menu do
		name 'Menu Name'
		sponsor 'HOTEL EASTMAN'
		event 'BREAKFAST'
		venue 'COMMERCIAL'
		place 'HOT SPRINGS, AR'
		physical_description 'CARD'
		occasion ''
	end
end